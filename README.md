# McScraper

This project is using Scrapy to scrape all the available URLs off a given website

# How to run?

Download the code and run using scrapy (https://scrapy.org/)

In order crawl a website you will need to use the following command
####scrapy crawl get_links -a url='www.example.com'

The URLs will be exported to a text file called parker.txt & logs to scrapy_test.log. 
These names can be changes by editing scrapy.cfg

