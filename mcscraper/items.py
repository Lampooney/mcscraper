import scrapy


class WebAddressItem(scrapy.Item):
    url = scrapy.Field()
