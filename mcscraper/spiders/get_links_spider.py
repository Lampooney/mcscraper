import scrapy
import re
from mcscraper.items import WebAddressItem
from mcscraper.logging import Logger
# scrapy crawl get_links -a url='www.southwalesbikes.co.uk'


class QuotesSpider(scrapy.Spider):
    name = "get_links"
    logger = Logger()

    def __init__(self, *args, **kwargs):
        # Use the url to set both the start_urls & allowed_domains
        super(QuotesSpider, self).__init__(*args, **kwargs)
        url = getattr(self, 'url', None)
        self.allowed_domains = [url]
        self.start_urls = ["http://" + url]

        # Initiate logging
        self.logger.set_logging()

    def parse(self, response):
        # Log the site we're about to visit
        message = 'Visting {}'.format(response.url)
        self.logger.write_to_log('INFO', message)
        # Get the hrefs from the anchor tag
        pages = response.css('a::attr(href)').extract()
        for page in pages:
            # Create the full page name
            full_page = response.urljoin(page)
            # Add the web address to the item
            item = WebAddressItem()
            item['url'] = full_page
            yield item

            # Check if the link is a picture - probably a better way to do this...
            # If it isn't then follow
            is_picture = re.findall("jpeg|jpg|png|gif$", full_page)
            if is_picture.__len__() == 0:
                yield scrapy.Request(full_page, callback=self.parse)
