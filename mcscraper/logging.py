import configparser
import logging


class Logger:
    def __init__(self):
        self.logger = logging.getLogger('scrapy')
        self.log_file = 'scrapy.log'
        self.logging_level = logging.ERROR

    def get_logging_level(self):
        config = configparser.ConfigParser()
        config.read('scrapy.cfg')
        self.log_file = config.get('logging', 'file_name')
        logging_level = config.get('logging', 'level')

        if logging_level == 'DEBUG':
            self.logging_level = logging.DEBUG
        elif logging_level == 'INFO':
            self.logging_level = logging.INFO
        elif logging_level == 'WARNING':
            self.logging_level = logging.WARNING
        elif logging_level == 'CRITICAL':
            self.logging_level = logging.CRITICAL
        else:
            self.logging_level = logging.ERROR

    def set_logging(self):
        self.get_logging_level()

        self.logger.setLevel(self.logging_level)
        # create file handler which logs even debug messages
        fh = logging.FileHandler(self.log_file)
        fh.setLevel(self.logging_level)
        # create console handler with a higher log level
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        # add the handlers to logger
        self.logger.addHandler(fh)

        return self.logger

    def write_to_log(self, level, message):

        # Log the message
        if level.lower() == 'critical':
            self.logger.critical(message)
        elif level.lower() == 'error':
            self.logger.error(message)
        elif level.lower() == 'warning':
            self.logger.warning(message)
        elif level.lower() == 'info':
            self.logger.info(message)
        else:
            self.logger.debug(message)