import unittest
import re


class Tests(unittest.TestCase):

    def test_only_visted_site_once(self):
        '''
        This test compares what I have logged as a website I have visited
        vs a unique list of those sites.  It currently fails as it visits
        the main site twice...
        '''
        pat = re.compile('(?<=Visting\s).*')
        with open('../../scrapy_test.log', 'r') as log:
            text = log.read()
            sites = pat.findall(text)
            dedup_list = list(set(sites))

            self.assertEqual(sites.__len__(), dedup_list.__len__())


if __name__ == "__main__":
    unittest.main()