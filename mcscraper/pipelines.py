# -*- coding: utf-8 -*-
from scrapy.exceptions import DropItem
import configparser


class DuplicateUrlPipeline(object):

    def __init__(self):
        self.urls_chosen = set()

    def process_item(self, item, spider):
        if item['url'] in self.urls_chosen:
            return
            # Commented this out as its polluting the log
            # raise DropItem("Duplicate item found: %s" % item)
        else:
            self.urls_chosen.add(item['url'])
            return item


class UrlWriterPipeline(object):

    def open_spider(self, spider):
        config = configparser.ConfigParser()
        config.read('scrapy.cfg')
        file_name = config.get('output', 'file_name')
        self.file = open(file_name, 'w')

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):
        # As we are not raising duplicate errors we need to check if item exists
        if item:
            line =  str(item.get('url')) + "\n"
            self.file.write(line)
            return item
        else:
            return